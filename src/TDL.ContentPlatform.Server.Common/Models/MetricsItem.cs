﻿using System;
using System.Diagnostics;
using TDL.ContentPlatform.Server.Common.Enums;

namespace TDL.ContentPlatform.Common.Models
{
    public class MetricsItem
    {
        public Guid MetricId { get; set; }
        public string ComputerName { get; set; }
        public Guid SessionId { get; set; }
        public DateTime TimeStamp { get; set; }
        public MetricsType MetricsType { get; set; }
        public string ClassName { get; set; }
        public string CallerMemberName { get; set; }
        public string CallerFilePath { get; set; }
        public int CallerLineNumber { get; set; }
        public string Action { get; set; }
        public string JsonParameters { get; set; }
        
    }
}
