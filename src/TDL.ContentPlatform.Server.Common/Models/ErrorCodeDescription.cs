﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDL.ContentPlatform.Server.Common.Models
{
    public class ErrorCodeDescription
    {
        public string Code { get; set; }
        public string Description { get; set; }

        public ErrorCodeDescription() { }

        public ErrorCodeDescription(string code, string description)
        {
            Code = code;
            Description = description;
        }
    }
}
