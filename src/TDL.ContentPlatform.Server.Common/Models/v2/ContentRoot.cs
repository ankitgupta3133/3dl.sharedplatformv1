﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Text;
using Newtonsoft.Json;
using TDL.ContentPlatform.Server.Common.Annotations;

namespace TDL.ContentPlatform.Server.Common.Models.v2
{
    public class ContentRoot
    {
        public virtual List<Grade> Grades { get; set; } = new List<Grade>();
        public virtual List<Language> Languages { get; set; } = new List<Language>();
        public virtual List<Activities> Activities { get; set; } = new List<Activities>();
        public virtual List<Compound> Compounds { get; set; } = new List<Compound>();
        public virtual List<Element> Elements { get; set; } = new List<Element>();
        public virtual List<Tag> Tags { get; set; } = new List<Tag>();
    }

    public class Compound
    {
        public int CompoundId { get; set; }
        public string CompoundName { get; set; }
        public string CommonName { get; set; }
        public string Color { get; set; }
        public string Material { get; set; }
        public string WikiLink { get; set; }
        public string Discription { get; set; }
        public int SphericalAssetId { get; set; }
        public string SphericalAsset { get; set; }
        public int BondedAssetId { get; set; }
        public string BondedAsset { get; set; }
        public List<Element> Elements { get; set; } = new List<Element>();
    }

    public class Element
    {
        public int ElementId { get; set; }
        public string ElementName { get; set; }
    }

    public class Activities
    {
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public List<ActivityItem> ActivityItem { get; set; } = new List<ActivityItem>();
    }

    public class ActivityItem
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public int Duration { get; set; }
        public bool? IsModelSpecific { get; set; }
        public bool? IsVisible { get; set; }
        public List<ActivityDetail> ActivityDetails { get; set; } = new List<ActivityDetail>();
        public List<ActivityContent> AssetContent { get; set; } = new List<ActivityContent>();
        public Dictionary<string, string> LocalizedName { get; set; } = new Dictionary<string, string>();
        public List<LocalName> ActivityLocal { get; set; } = new List<LocalName>();
    }

    public class ClassificationItem
    {
        public int ClassificationId { get; set; }
        public string ClassificationName { get; set; }
        public List<PropertyItem> Properties { get; set; } = new List<PropertyItem>();
        public List<ActivityDetail> ClassificationDetails { get; set; } = new List<ActivityDetail>();
        public Dictionary<string, string> LocalizedName { get; set; } = new Dictionary<string, string>();
    }

    public class PropertyItem
    {
        public int ItemId { get; set; }
        public int PropertyId { get; set; }
        public string PropertyName { get; set; }
        public List<ModelItem> ModelDetails { get; set; } = new List<ModelItem>();
        //public List<LocalizedNameItem> LocalizedName { get; set; } = new List<LocalizedNameItem>();
        public Dictionary<string, string> LocalizedName { get; set; } = new Dictionary<string, string>();
    }

    public class LocalizedNameItem
    {
        public string CultureCode { get; set; }
        public string Name { get; set; }
    }

    public class ModelItem
    {
        public int ItemDetailId { get; set; }
        public int AssetId { get; set; }
        public string AssetName { get; set; }
        public int AssetContentId { get; set; }
    }

    public class ActivityDetail
    {
        public Guid PackageId { get; set; }
        public Guid LanguageId { get; set; }
        public Guid GradeId { get; set; }
        public Guid SyllabusId { get; set; }
        public Guid StreamId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TopicId { get; set; }
        public Guid SubtopicId { get; set; }
    }

    public class ActivityContent
    {
        public int ItemDetailId { get; set; }
        public int NumberOfLabels { get; set; }
        public int NumberOfAttempts { get; set; }
        public int AssetId { get; set; }
        public int AssetContentId { get; set; }
        public List<QuizLabels> QuizLabels { get; set; } = new List<QuizLabels>();
    }

    public class Grade
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual List<Stream> Streams { get; set; } = new List<Stream>();
        public virtual List<Subject> Subjects { get; set; } = new List<Subject>();
        public Dictionary<string, int> RecursiveTypeCount { get; set; } = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        public List<Asset> Assets { get; set; } = new List<Asset>();

        public string ThumbnailHash { get; set; }
        public List<CdnUrl> ThumbnailCdnUrls { get; set; } = new List<CdnUrl>();
        //public string ThumbnailUrl { get; set; }
    }

    public class Stream
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Subject> Subjects { get; set; } = new List<Subject>();
        public List<Asset> Assets { get; set; } = new List<Asset>();
    }

    public class Subject
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Topic> Topics { get; set; } = new List<Topic>();
        public List<Asset> Assets { get; set; } = new List<Asset>();
        public Dictionary<string, int> RecursiveTypeCount { get; set; } = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        public string ThumbnailHash { get; set; }
        public List<CdnUrl> ThumbnailCdnUrls { get; set; } = new List<CdnUrl>();
        public List<AssetCountByType> AssetCountByType { get; set; } = new List<AssetCountByType>();
        //To exclude AssetCountByType from json serialization
        public bool ShouldSerializeAssetCountByType() { return false; }
        //public string ThumbnailUrl { get; set; }
    }

    public class Topic
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Subtopic> Subtopics { get; set; } = new List<Subtopic>();
        public List<Asset> Assets { get; set; } = new List<Asset>();
        public Dictionary<string, int> RecursiveTypeCount { get; set; } = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        public string ThumbnailHash { get; set; }
        public List<CdnUrl> ThumbnailCdnUrls { get; set; } = new List<CdnUrl>();
        public List<AssetCountByType> AssetCountByType { get; set; } = new List<AssetCountByType>();
        //To exclude AssetCountByType from json serialization
        public bool ShouldSerializeAssetCountByType() { return false; }
        //public string ThumbnailUrl { get; set; }
    }

    public class AssetCountByType
    {
        public string Type { get; set; }
        public int Count { get; set; }
    }

    public class Subtopic
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<Asset> Assets { get; set; } = new List<Asset>();
        public Dictionary<string, int> RecursiveTypeCount { get; set; } = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase);
        public string ThumbnailHash { get; set; }
        public List<CdnUrl> ThumbnailCdnUrls { get; set; } = new List<CdnUrl>();
        public List<AssetCountByType> AssetCountByType { get; set; } = new List<AssetCountByType>();
        //public string ThumbnailUrl { get; set; }
        //To exclude AssetCountByType from json serialization
        public bool ShouldSerializeAssetCountByType() { return false; }
    }
    public class Asset
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        //public string Tag { get; set; }
        public List<TagContent> Tags { get; set; }
        public List<AssetContent> AssetContents { get; set; } = new List<AssetContent>();
        public List<AssociatedContent> AssociatedContents { get; set; } = new List<AssociatedContent>();
        public Dictionary<string, string> LocalizedName { get; set; } = new Dictionary<string, string>();
        public virtual List<Language> Languages { get; set; } = new List<Language>();
        public string VimeoUrl { get; set; }
        public List<LocalName> AssetLocal { get; set; } = new List<LocalName>();
    }
    public class AssetContent
    {
        public int Id { get; set; }
        public string Platform { get; set; }
        public string PlatformId { get; set; }
        public AssetContentMetadata Metadata { get; set; }
        //public List<Activity> Activities { get; set; } = new List<Activity>();
        public Dictionary<string, string> LocalizedDescription { get; set; } = new Dictionary<string, string>();
        public int Version { get; set; }
        public string ThumbnailHash { get; set; }
        public List<CdnUrl> ThumbnailCdnUrls { get; set; } = new List<CdnUrl>();
        public string BackgroundHash { get; set; }
        public List<CdnUrl> BackgroundCdnUrls { get; set; } = new List<CdnUrl>();
        //public string ThumbnailUrl { get; set; }
        public string FileHash { get; set; }
        public string FileSize { get; set; }
        public List<CdnUrl> FileCdnUrls { get; set; } = new List<CdnUrl>();

        //public string ThumbnailUrl { get; set; }
        public bool IsVisible;

    }

    public class AssetContentMetadata
    {
        public Model3DLabels Model3DLabels { get; set; } = new Model3DLabels();
    }

    public class AssociatedContent
    {
        public int Id { get; set; }
        public int ModelId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string PlatformId { get; set; }
        public int Version { get; set; }
        public string ThumbnailHash { get; set; }
        public List<CdnUrl> ThumbnailCdnUrls { get; set; }
        public string BackgroundHash { get; set; }
        public List<CdnUrl> BackgroundCdnUrls { get; set; }
        public string FileHash { get; set; }
        public string FileSize { get; set; }
        public List<CdnUrl> FileCdnUrls { get; set; } = new List<CdnUrl>();
        public virtual List<Language> Languages { get; set; } = new List<Language>();
    }
    public class Activity
    {
        public int ActivityId { get; set; }
        public string ActivityName { get; set; }
        public List<ModelActivity> ActivityItem { get; set; } = new List<ModelActivity>();
    }

    public class ModelActivity
    {
        public int ItemId { get; set; }
        public string ItemName { get; set; }
        public bool? IsModelSpecific { get; set; }
        public int ItemDetailId { get; set; }
        public int NumberOfLabels { get; set; }
        public int NumberOfAttempts { get; set; }
        public bool? IsVisible { get; set; }
        public List<QuizLabels> QuizLabels { get; set; } = new List<QuizLabels>();
    }

    public class QuizLabels
    {
        public int LabelId { get; set; }
        public string LabelName { get; set; }
    }

    public class Model3DLabels : INotifyPropertyChanged
    {
        private List<Model3DLabelItem> _items;

        public List<Model3DLabelItem> Items
        {
            get { return _items; }
            set
            {
                if (Equals(value, _items)) return;
                _items = value;
                OnPropertyChanged();
            }
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    public class Model3DLabelItem : INotifyPropertyChanged
    {
        private bool _isVisible;
        private Model3DVector3 _position;
        private Model3DVector3 _rotation;
        private Model3DVector3 _scale;
        private string _modelName;
        private string _highlightColor;
        private string _originalColor;
        //private bool _enabled;

        public string ModelName
        {
            get { return _modelName; }
            set
            {
                if (value == _modelName) return;
                _modelName = value;
                OnPropertyChanged();
            }
        }

        public Model3DVector3 Position
        {
            get { return _position; }
            set
            {
                if (Equals(value, _position)) return;
                _position = value;
                OnPropertyChanged();
            }
        }

        public Model3DVector3 Rotation
        {
            get { return _rotation; }
            set
            {
                if (Equals(value, _rotation)) return;
                _rotation = value;
                OnPropertyChanged();
            }
        }

        public Model3DVector3 Scale
        {
            get { return _scale; }
            set
            {
                if (Equals(value, _scale)) return;
                _scale = value;
                OnPropertyChanged();
            }
        }

        public int LabelId { get; set; }

        public string HighlightColor
        {
            get { return _highlightColor; }
            set
            {
                if (value == _highlightColor) return;
                _highlightColor = value;
                OnPropertyChanged();
            }
        }

        public string OriginalColor
        {
            get { return _originalColor; }
            set
            {
                if (value == _originalColor) return;
                _originalColor = value;
                OnPropertyChanged();
            }
        }

        public Dictionary<string, string> LocalizedText { get; set; } = new Dictionary<string, string>();
        public List<LocalName> LabelLocal { get; set; } = new List<LocalName>();
        public List<LocalName> ModelLocal { get; set; } = new List<LocalName>();

        /// <summary>
        /// This property is for internal use only
        /// </summary>
        [JsonIgnore]
        public bool IsVisible
        {
            get { return _isVisible; }
            set
            {
                if (value == _isVisible) return;
                _isVisible = value;
                OnPropertyChanged();
            }
        }

        //public bool Enabled
        //{
        //    get { return _enabled; }
        //    set
        //    {
        //        if (value == _enabled) return;
        //        _enabled = value;
        //        OnPropertyChanged();
        //    }
        //}


        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    public class Model3DVector3
    {
        private static readonly CultureInfo ParseCulture = new CultureInfo("en-US");
        private float _x;
        private float _y;
        private float _z;

        public Model3DVector3(string x, string y, string z)
        {
            if (x != null)
                float.TryParse(x.Replace(",", "."), NumberStyles.Float, ParseCulture, out _x);
            if (y != null)
                float.TryParse(y.Replace(",", "."), NumberStyles.Float, ParseCulture, out _y);
            if (z != null)
                float.TryParse(z.Replace(",", "."), NumberStyles.Float, ParseCulture, out _z);
        }

        public float X
        {
            get { return _x; }
            set { _x = value; }
        }

        public float Y
        {
            get { return _y; }
            set { _y = value; }
        }

        public float Z
        {
            get { return _z; }
            set { _z = value; }
        }
    }

    public class Language
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string CultureCode { get; set; }
    }
    public class Syllabus
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }

    public class CdnUrl
    {
        public string Url { get; set; }
    }

    public class Apk
    {
        public string ApkName { get; set; }
        public string ApkVersion { get; set; }
        public bool IsMandatory { get; set; }
        public List<CdnUrl> ApkCdnUrls { get; set; }
    }

    public class Tag
    {
        public int id { get; set; }
        public TagName[] names { get; set; }
    }

    public class TagContent
    {
        public int id { get; set; }
    }

    public class TagName
    {
        public string language { get; set; }
        public string name { get; set; }
    }

    public class LocalName
    {
        public string culture { get; set; }
        public string name { get; set; }
    }

    public class AppMessage
    {
        public string messageCode { get; set; }        
        public virtual List<Message> messageLocal { get; set; }
    }

    public class Message
    {
        public string culture { get; set; }
        public string name { get; set; }
    }
}
