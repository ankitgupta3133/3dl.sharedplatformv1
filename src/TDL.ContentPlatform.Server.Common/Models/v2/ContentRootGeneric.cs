﻿//using System;
//using System.Collections.Generic;

//namespace TDL.ContentPlatform.Server.Common.Models.v2.GenericTest
//{
//    public class DefaultContentRoot : ContentRoot<Language, Syllabus,
//        Grade<Stream<Subject<Topic<Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Topic<Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Subject<Topic<Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Topic<Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>,
//        Stream<Subject<Topic<Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Topic<Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>,
//        Subject<Topic<Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>, Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>,
//        Topic<Subtopic<Asset<AssetContent>, AssetContent>, Asset<AssetContent>, AssetContent>,
//        Subtopic<Asset<AssetContent>, AssetContent>,
//        Asset<AssetContent>,
//        AssetContent>
//    {

//    }

//    public class ContentRoot<TLanguage, TSyllabus, TGrade, TStream, TSubject, TTopic, TSubtopic, TAsset, TAssetContent>
//        where TLanguage : Language
//        where TSyllabus : Syllabus
//        where TGrade : Grade<TStream, TSubject, TTopic, TSubtopic, TAsset, TAssetContent>
//        where TStream : Stream<TSubject, TTopic, TSubtopic, TAsset, TAssetContent>
//        where TSubject : Subject<TTopic, TSubtopic, TAsset, TAssetContent>
//        where TTopic : Topic<TSubtopic, TAsset, TAssetContent>
//        where TSubtopic : Subtopic<TAsset, TAssetContent>
//        where TAsset : Asset<TAssetContent>
//        where TAssetContent : AssetContent
//    {
//        public virtual Dictionary<Guid,TGrade> Grades { get; set; } = new Dictionary<Guid,TGrade>();
//        public virtual Dictionary<Guid,TLanguage> Languages { get; set; } = new Dictionary<Guid,TLanguage>();
//    }

//    public class Grade<TStream, TSubject, TTopic, TSubtopic, TAsset, TAssetContent>
//        where TStream : Stream<TSubject, TTopic, TSubtopic, TAsset, TAssetContent>
//        where TSubject : Subject<TTopic, TSubtopic, TAsset, TAssetContent>
//        where TTopic : Topic<TSubtopic, TAsset, TAssetContent>
//        where TSubtopic : Subtopic<TAsset, TAssetContent>
//        where TAsset : Asset<TAssetContent>
//        where TAssetContent : AssetContent
//    {
//        public Guid Id { get; set; }
//        public virtual Dictionary<Guid,TStream> Streams { get; set; } = new Dictionary<Guid,TStream>();
//    }

//    public class Stream<TSubject, TTopic, TSubtopic, TAsset, TAssetContent>
//        where TSubject : Subject<TTopic, TSubtopic, TAsset, TAssetContent>
//        where TTopic : Topic<TSubtopic, TAsset, TAssetContent>
//        where TSubtopic : Subtopic<TAsset, TAssetContent>
//        where TAsset : Asset<TAssetContent>
//        where TAssetContent : AssetContent
//    {
//        public Guid Id { get; set; }
//        public virtual Dictionary<Guid,TSubject> Subjects { get; set; } = new Dictionary<Guid,TSubject>();
//    }

//    public class Subject<TTopic, TSubtopic, TAsset, TAssetContent>
//        where TTopic : Topic<TSubtopic, TAsset, TAssetContent>
//        where TSubtopic : Subtopic<TAsset, TAssetContent>
//        where TAsset : Asset<TAssetContent>
//        where TAssetContent : AssetContent
//    {
//        public Guid Id { get; set; }
//        public virtual Dictionary<Guid,TTopic> Topics { get; set; } = new Dictionary<Guid,TTopic>();
//    }

//    public class Topic<TSubtopic, TAsset, TAssetContent>
//        where TSubtopic : Subtopic<TAsset, TAssetContent>
//        where TAsset : Asset<TAssetContent>
//        where TAssetContent : AssetContent
//    {
//        public Guid Id { get; set; }
//        public virtual Dictionary<Guid,TSubtopic> Topics { get; set; } = new Dictionary<Guid,TSubtopic>();
//    }

//    public class Subtopic<TAsset, TAssetContent>
//        where TAsset : Asset<TAssetContent>
//        where TAssetContent : AssetContent
//    {
//        public Guid Id { get; set; }
//    }
//    public class Asset<TAssetContent>
//        where TAssetContent : AssetContent
//    {
//        public Guid Id { get; set; }
//    }
//    public class AssetContent
//    {
//        public Guid Id { get; set; }
//    }
//    public class Language
//    {
//        public Guid Id { get; set; }
//        public string Name { get; set; }
//        public string CultureCode { get; set; }
//    }
//    public class Syllabus
//    {
//        public Guid Id { get; set; }
//        public string Name { get; set; }
//        public string Description { get; set; }
//    }
//}
