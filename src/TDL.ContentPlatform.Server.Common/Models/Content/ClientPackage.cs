﻿using System;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientPackage {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<ClientLanguage> Languages { get; set; }
        public List<ClientGrade> Grades { get; set; }
        public List<ClientAsset> Assets { get; set; }
    }
}