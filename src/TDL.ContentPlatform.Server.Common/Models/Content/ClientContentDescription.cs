﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientContentDescription
    {
        public Guid LanguageId { get; set; }
        public string Description { get; set; }
    }
}