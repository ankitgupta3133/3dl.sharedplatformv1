﻿using System;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientAsset
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string AssetType { get; set; }
        public string Status { get; set; }
        public string Language { get; set; }
        public string Grade { get; set; }
        public string Syllabus { get; set; }
        public string Stream { get; set; }
        public string Subject { get; set; }
        public string Topic { get; set; }
        public string Subtopic { get; set; }
        public string Tags { get; set; }
        public bool IsQuizEnabled { get; set; }
        public decimal PercentageOfLabels { get; set; }
        public int NumberOfAttempts { get; set; }
        public List<ClientAssetContent> Contents { get; set; }
        public List<ClientAssetName> Names { get; set; }
        public List<ClientAssetDetail> Details { get; set; }
        public List<Guid> GradeIds { get; set; }
        public List<Guid> SyllabusIds { get; set; }
        public List<Guid> StreamsIds { get; set; }
        public List<Guid> SubjectIds { get; set; }
        public List<Guid> TopicIds { get; set; }
        public List<Guid> SubTopicIds { get; set; }
        public List<Guid> LanguageIds { get; set; }
    }
}