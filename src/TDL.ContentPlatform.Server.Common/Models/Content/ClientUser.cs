﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientUser
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Role { get; set; }
    }
}
