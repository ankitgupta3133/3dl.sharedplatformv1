﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientLanguage
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}