﻿namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class FavoriteAsset {
        public int AssetId { get; set; }
        public string AssetName { get; set; }
    }
}