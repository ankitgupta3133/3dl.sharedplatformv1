﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientActivityDetail
    {
        public Guid PackageId { get; set; }
        public Guid LanguageId { get; set; }
        public Guid GradeId { get; set; }
        public Guid SyllabusId { get; set; }
        public Guid StreamId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TopicId { get; set; }
        public Guid SubtopicId { get; set; }
    }
}