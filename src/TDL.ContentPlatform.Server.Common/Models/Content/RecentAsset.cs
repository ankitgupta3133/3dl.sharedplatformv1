﻿namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class RecentAsset
    {
        public int AssetId { get; set; }
        public string AssetName { get; set; }
    }
}