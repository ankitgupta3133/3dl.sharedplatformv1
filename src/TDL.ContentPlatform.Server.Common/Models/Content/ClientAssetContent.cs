﻿using System;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientAssetContent
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string FileSize { get; set; }
        public int Version { get; set; }
        public string Platform { get; set; }
        public string PlatformId { get; set; }
        public string Url { get; set; }
        public string Thumbnail { get; set; }
        public List<ClientContentDetail> Details { get; set; }
        public List<ClientContentDescription> Description { get; set; }
        public List<ClientContentWhatsNew> WhatsNew { get; set; }
        public string ThumbnailUrl { get; set; }
        public string LabelXml { get; set; }
        public string LabelXmlUrl { get; set; }
        public Guid PackageId { get; set; }
    }
}