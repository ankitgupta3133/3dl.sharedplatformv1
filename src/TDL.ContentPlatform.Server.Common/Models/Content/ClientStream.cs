﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientStream
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}