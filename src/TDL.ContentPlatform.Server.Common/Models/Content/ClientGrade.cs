﻿using System;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientGrade
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<ClientSyllabus> Syllabus { get; set; }
        public List<ClientStream> Streams { get; set; }
        public List<ClientSubject> Subjects { get; set; }
    }
}