﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientAssetName
    {
        public string Name { get; set; }
        public Guid LanguageId { get; set; }
    }
}