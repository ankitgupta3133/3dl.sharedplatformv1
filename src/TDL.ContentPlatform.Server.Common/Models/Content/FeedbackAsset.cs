﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class FeedbackAsset
    {
        public int FeedbackId { get; set; }
        public string Feedback { get; set; }
        public int AssetContentId { get; set; }
        public Guid UserId { get; set; }
        public string Username { get; set; }
        public Guid? GradeId { get; set; }
        public string Grade { get; set; }
        public Guid? SubjectId { get; set; }
        public string Subject { get; set; }
        public Guid? TopicId { get; set; }
        public string Topic { get; set; }
        public Guid? SubtopicId { get; set; }
        public string Subtopic { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}