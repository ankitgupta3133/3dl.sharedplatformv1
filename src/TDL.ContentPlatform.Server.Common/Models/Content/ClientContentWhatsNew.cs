﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientContentWhatsNew
    {
        public Guid LanguageId { get; set; }
        public string WhatsNew { get; set; }
    }
}