﻿using System;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountRegisterResponse:ResponseBase
    {
        public Guid UserId { get; set; }
        public List<ErrorCodeDescription> Errors { get; set; }
    }
}