﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class RecentAddAssetRequest
    {
        public int AssetID { get; set; }
    }
}