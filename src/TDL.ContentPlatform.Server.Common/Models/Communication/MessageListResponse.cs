﻿using System;
using System.Collections.Generic;
using System.Text;
using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
   public class MessageListResponse : ResponseBase
    {
        public virtual List<AppMessage> Messages { get; set; } = new List<AppMessage>();
    }
}
