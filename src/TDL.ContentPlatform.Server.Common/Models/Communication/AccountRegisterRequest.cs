﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountRegisterRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public bool IsDemoAccount { get; set; }
        public int Duration { get; set; }
        public Guid Account { get; set; }
        public string[] Roles { get; set; }
    }
}