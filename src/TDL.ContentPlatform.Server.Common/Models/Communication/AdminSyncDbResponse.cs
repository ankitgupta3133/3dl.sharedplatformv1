﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AdminSyncDbResponse : ResponseBase

    {
        public List<string> Log { get; set; }=new List<string>();
    }
}