﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AppMasterResponse : ResponseBase
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}