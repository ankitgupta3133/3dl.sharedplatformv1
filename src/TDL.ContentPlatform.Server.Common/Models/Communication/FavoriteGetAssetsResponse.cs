﻿using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class FavoriteGetAssetsResponse : ResponseBase
    {
        //public List<FavoriteAsset> Assets { get; set; }
        public ContentRoot ContentRoot { get; set; }
    }
}