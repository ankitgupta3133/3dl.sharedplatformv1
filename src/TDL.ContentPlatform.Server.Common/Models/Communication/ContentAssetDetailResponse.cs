﻿using System;
using System.Collections.Generic;
using System.Text;
using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ContentAssetDetailResponse : ResponseBase
    {
        public List<Asset> Assets { get; set; }
    }
}
