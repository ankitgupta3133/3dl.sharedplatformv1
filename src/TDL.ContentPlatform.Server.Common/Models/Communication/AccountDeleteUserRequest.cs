﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountDeleteUserRequest
    {
        public string Username { get; set; }
        public string Id { get; set; }
    }
}