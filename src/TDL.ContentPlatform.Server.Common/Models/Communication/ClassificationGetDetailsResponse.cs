﻿using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ClassificationGetDetailsResponse : ResponseBase
    {
        public ClassificationItem Classification { get; set; }
    }
}