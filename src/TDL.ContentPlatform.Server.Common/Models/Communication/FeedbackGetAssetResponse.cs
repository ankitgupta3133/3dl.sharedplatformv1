﻿using System.Collections.Generic;
using TDL.ContentPlatform.Server.Common.Models.Content;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class FeedbackGetAssetResponse : ResponseBase
    {
        public List<FeedbackAsset> Feedback { get; set; }
    }
}