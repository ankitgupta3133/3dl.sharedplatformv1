﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class RecentGetAssetsRequest
    {
        public bool IncludeEmptyLevels { get; set; } = false;
        public string Platform { get; set; }
        public string ClientName { get; set; }
        public string ClientVersion { get; set; }
    }
}