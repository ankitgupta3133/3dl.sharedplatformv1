﻿using System.Collections.Generic;
using TDL.ContentPlatform.Server.Common.Models.Content;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ContentPackageListResponse: ResponseBase

    {
        public List<ClientPackage> Packages { get; set; }
    }
}
