﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class FavoriteRemoveAssetRequest
    {
        public int AssetID { get; set; }
    }
}