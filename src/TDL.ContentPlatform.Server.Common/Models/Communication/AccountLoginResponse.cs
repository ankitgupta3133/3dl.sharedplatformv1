﻿using System.Linq;
using TDL.ContentPlatform.Server.Common.Models.Content;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountLoginResponse:ResponseBase
    {
        public string AuthorizationToken { get; set; }
        public ClientUser User { get; set; }

        public string FirstLetter
        {
            get { return (User?.Firstname?.First() ?? '?').ToString().ToUpper(); }
        }
    }
}