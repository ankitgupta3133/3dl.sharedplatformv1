﻿using TDL.ContentPlatform.Server.Common.Enums;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ConfigKeyValuePair
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public DataTypeEnum DataType { get; set; }
    }
}