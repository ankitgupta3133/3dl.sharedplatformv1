﻿using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ApkDetailResponse : ResponseBase
    {
        public virtual Apk Apk { get; set; } = new Apk();
    }
}