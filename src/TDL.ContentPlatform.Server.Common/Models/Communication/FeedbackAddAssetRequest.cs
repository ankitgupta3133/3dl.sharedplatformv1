﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class FeedbackAddAssetRequest
    {
        public int AssetContentID { get; set; }
        public string Feedback { get; set; }
        //public Guid SubscriptionId { get; set; }
        //public Guid PackageId { get; set; }
        //public Guid LanguageId { get; set; }
        public Guid GradeId { get; set; }
        //public Guid SyllabusId { get; set; }
        //public Guid StreamId { get; set; }
        public Guid SubjectId { get; set; }
        public Guid TopicId { get; set; }
        public Guid SubtopicId { get; set; }
        public string Status { get; set; }
    }
}