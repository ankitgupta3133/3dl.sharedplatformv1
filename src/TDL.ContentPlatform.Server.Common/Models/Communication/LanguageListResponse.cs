﻿using System.Collections.Generic;
using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class LanguageListResponse : ResponseBase
    {
        public virtual List<Language> Languages { get; set; } = new List<Language>();
    }
}