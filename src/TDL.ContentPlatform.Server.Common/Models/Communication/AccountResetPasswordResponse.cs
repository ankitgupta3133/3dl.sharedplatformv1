﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountResetPasswordResponse:ResponseBase
    {
        public List<ErrorCodeDescription> Errors { get; set; }
    }
}