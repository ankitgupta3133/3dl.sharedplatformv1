﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ContentPublishingUnpublishRequest
    {
        public int Id { get; set; }
        public string Password { get; set; }
        public Guid UserId { get; set; }
    }
}