﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ClassificationGetDetailsRequest
    {
        public int ClassificationId { get; set; }
    }
}