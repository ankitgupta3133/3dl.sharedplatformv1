﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class FeedbackGetAssetRequest
    {
        public int AssetContentID { get; set; } 
    }
}