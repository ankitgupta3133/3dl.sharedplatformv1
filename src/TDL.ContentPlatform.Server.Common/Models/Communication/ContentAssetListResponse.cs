﻿using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ContentAssetListResponse:ResponseBase
    {
        public ContentRoot ContentRoot { get; set; }
    }
}