﻿using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ContentEditMetadataRequest
    {
        public int AssetContentId { get; set; }
        public string Json { get; set; }
        public string Type { get; set; }
    }
}