﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class MetricsClientFileUploadResponse:ResponseBase
    {
        public int FileCount { get; set; }
        public int TotalCompressedSize { get; set; }
        public int TotalUncompressedSize { get; set; }
    }
}