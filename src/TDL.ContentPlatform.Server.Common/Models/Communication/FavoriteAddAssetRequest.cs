﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class FavoriteAddAssetRequest
    {
        public int AssetID { get; set; }
    }
}