﻿using TDL.ContentPlatform.Common;
using TDL.ContentPlatform.Server.Common.Utils;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ContentAssetListRequest
    {
        public bool IncludeEmptyLevels { get; set; } = false;
        public string Platform { get; set; }
        public string ModulePlatform { get; set; }
        public string ClientName { get; set; }
        public string ClientVersion { get; set; }
        public bool isDetail { get; set; } = true;
    }

    public class ContentAssetDetailRequest
    {
        public string EntityId { get; set; }
        public string Platform { get; set; }        
        public EntityType EntityType { get; set; }
        public string ClientVersion { get; set; }
    }    
}