﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AppMasterRequest
    {
        public string AppId { get; set; }
    }
}