﻿using System.Collections.Generic;
using TDL.ContentPlatform.Server.Common.Models.Content;
using TDL.ContentPlatform.Server.Common.Models.v2;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class RecentGetAssetsResponse : ResponseBase
    {
        //public List<RecentAsset> Assets { get; set; }
        public ContentRoot ContentRoot { get; set; }
    }
}