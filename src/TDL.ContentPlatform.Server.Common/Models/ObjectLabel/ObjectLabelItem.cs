﻿namespace TDL.ContentPlatform.Server.Common.Models.ObjectLabel
{
    public class ObjectLabelItem
    {
        public int Face { get; set; }
        public int Number { get; set; }
        public ObjectLabelLanguageText Text { get; set; }

        public ObjectLabelColor Color { get; set; }
        public ObjectLabelVector3 PopupPosition { get; set; }
        public ObjectLabelVector3 PopupRotation { get; set; }
        public ObjectLabelVector3 PopupScale { get; set; }

    }
}