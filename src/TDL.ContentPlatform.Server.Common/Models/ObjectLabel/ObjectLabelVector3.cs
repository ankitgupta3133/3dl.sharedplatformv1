﻿namespace TDL.ContentPlatform.Server.Common.Models.ObjectLabel
{
    public class ObjectLabelVector3
    {
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }
    }
}