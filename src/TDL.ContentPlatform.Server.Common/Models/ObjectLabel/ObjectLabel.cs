﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.ObjectLabel
{
    public class ObjectLabel
    {
        public List<ObjectLabelModel> Models { get; set; } = new List<ObjectLabelModel>();
    }
}
