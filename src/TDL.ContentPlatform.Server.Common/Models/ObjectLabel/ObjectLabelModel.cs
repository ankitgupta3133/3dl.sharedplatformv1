﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.ObjectLabel
{
    public class ObjectLabelModel
    {
        public ObjectLabelLanguageText Name { get; set; }
        public ObjectLabelVector3 HidingPosition { get; set; }
        public List<ObjectLabelItem> Items { get; set; } = new List<ObjectLabelItem>();
    }
}