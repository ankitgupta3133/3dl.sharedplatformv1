﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Newtonsoft.Json;
using TDL.ContentPlatform.Server.Common.Models.ObjectLabel;

namespace TDL.ContentPlatform.Server.Common.Utils
{
    public static class ObjectLabelTools
    {
        public static string Serialize(ObjectLabel objectLabel)
        {
            return JsonConvert.SerializeObject(objectLabel);
        }

        public static ObjectLabel Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<ObjectLabel>(json);
        }

        //public static ObjectLabel DeserializeXml(string xml)
        //{
        //    var ret = new ObjectLabel();

        //    var LabelDoc = new XmlDocument();
        //    LabelDoc.LoadXml(xml);
        //    var root = LabelDoc.DocumentElement;
        //    var nodes = root.SelectNodes("model");


        //    //PageLabel = new PageLabels();
        //    //PageLabel.PageLabelData = new List<PageLabelList>();


        //    foreach (XmlNode node in nodes)
        //    {
        //        var model = new ObjectLabelModel();
        //        ret.Models.Add(model);

        //        // var objPageList = new PageLabelList();
        //        model.Name = new LanguageText();
        //        if (node.Attributes != null)
        //        {
        //            model.Name.Languages.Add("EN", node.Attributes["EN"].Value);
        //            model.Name.Languages.Add("NO", node.Attributes["NO"].Value);
        //            model.Name.Languages.Add("AR", node.Attributes["AR"].Value);
        //        }

        //        model.HidingPosition = new ObjectLabelVector3()
        //        {
        //            X=float.Parse()
        //        };


        //        objPageList.PageName = node.Attributes["name"].Value;
        //        objPageList.pageLabelitem = new List<PageLabelItem>();
        //        foreach (XmlNode cNode in node)
        //        {
        //            PageLabelItem objPageLabelItem = new PageLabelItem();
        //            objPageLabelItem.labelID = Convert.ToInt32(cNode.Attributes["labelid"].Value);
        //            objPageLabelItem.labelText = cNode.Attributes["labeltext"].Value;
        //            objPageLabelItem.language = cNode.Attributes["language"].Value;

        //            objPageList.pageLabelitem.Add(objPageLabelItem);
        //        }
        //        PageLabel.PageLabelData.Add(objPageList);

        //    }}
    }
}
