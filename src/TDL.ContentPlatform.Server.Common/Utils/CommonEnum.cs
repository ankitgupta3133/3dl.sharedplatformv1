﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TDL.ContentPlatform.Server.Common.Utils
{
    public enum EntityType
    {       
        Subject,
        Topic,
        Subtopic
    }

    public enum ErrorCode
    {
        AccountDeactivated,
        ChangePasswordDenied,
        IncorrectPassword,
        InternalErrorChangePassword,
        InternalErrorLogin,
        InternalErrorPassword,
        InternalErrorRegister,
        InternalErrorResendInvitationProcedure,
        InternalErrorResendPassword,
        InternalErrorResetPasswordLink,
        InvalidFileUploadExtension,
        InvalidPlatform,
        InvalidUserPlatform,
        LoginDenied,
        NoApkFound,
        NoClassificationFound,
        PlatformAllNotExist,
        PlatformNameTooLong,
        ProcessingAssetListRequest,
        RegisterDenied,
        RequestDenied,
        ResendInvitationDenied,
        ResendPasswordDenied,
        ResetPasswordDenied,
        UploadFileSizeLimit,
        UserDeleted,
        UserLocked,
        UserNotFound
    }

    public enum MessageType
    {
        Error = 1,
        AppLabel = 2
    }
}
