﻿namespace TDL.ContentPlatform.Server.Common.Enums
{
    public enum MetricsType
    {
        Action = 0,
        InternalError = 1,
        ApplicationStart = 10,
        ApplicationRunning = 11,
        ApplicationStop = 12,
        ApplicationQuit = 13,

        Login = 100,
        LoginResult = 101,
        ChangePage = 200,
        SelectGrade = 300,


        SelectSubject,
        SelectTopic,
        SelectSubtopic,
        StartContentPresentation,
        StopContentPresentation,
        Standard3DModel_Load,
        VideoPresenter_Load,
        ApiAccess,
        ChangeLanguage
    }
}
