﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDL.ContentPlatform.Server.Common.Enums
{
    public enum KnownUserRolesEnum
    {
        Unknown = 0,
        Administrator = 100,
        Student = 1000
    }
}
