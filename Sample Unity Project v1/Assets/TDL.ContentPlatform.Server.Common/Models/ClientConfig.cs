﻿namespace TDL.ContentPlatform.Server.Common.Models
{
    public class ClientConfig
    {
        public string ServerUrl { get; set; }
        public string ContentDir { get; set; }
    }
}
