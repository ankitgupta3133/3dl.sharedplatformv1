﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountLoginResponse:ResponseBase
    {
        public string AuthorizationToken { get; set; }
    }
}