﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public abstract class ResponseBase
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
    }
}