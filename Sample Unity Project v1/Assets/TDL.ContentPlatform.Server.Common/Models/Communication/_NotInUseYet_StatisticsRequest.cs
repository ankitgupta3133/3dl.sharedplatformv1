﻿using System;
using TDL.ContentPlatform.Server.Common.Enums;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class _NotInUseYet_StatisticsRequest
    {
        /// <summary>
        /// Unique identifier for this package
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Unique identifier for this session
        /// </summary>
        public Guid Session { get; set; }

        // Category
        public string Category { get; set; }
        public string Name { get; set; }
        
        // Payload
        public DataTypeEnum DataType { get; set; }
        public string StringData { get; set; }
        public long LongData { get; set; }
        public double DoubleData { get; set; }
    }
}
