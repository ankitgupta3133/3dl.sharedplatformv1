﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountResetPasswordRequest
    {
        public string Username { get; set; }
    }
}