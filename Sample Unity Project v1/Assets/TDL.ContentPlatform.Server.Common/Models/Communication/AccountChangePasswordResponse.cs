﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountChangePasswordResponse : ResponseBase
    {
        public List<ErrorCodeDescription> Errors { get; set; }
    }
}