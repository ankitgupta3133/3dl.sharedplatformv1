﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class ConfigConfigurationResponse:ResponseBase
    {
        public List<ConfigKeyValuePair> ConfigKeyValuePair { get; set; }
    }
}