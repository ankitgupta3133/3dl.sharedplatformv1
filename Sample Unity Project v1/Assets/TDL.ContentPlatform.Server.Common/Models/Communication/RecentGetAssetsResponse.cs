﻿using System.Collections.Generic;
using TDL.ContentPlatform.Server.Common.Models.Content;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class RecentGetAssetsResponse : ResponseBase
    {
        public List<RecentAsset> Assets { get; set; }
    }
}