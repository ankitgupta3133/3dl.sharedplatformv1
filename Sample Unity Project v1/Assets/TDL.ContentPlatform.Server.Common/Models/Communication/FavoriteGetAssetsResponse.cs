﻿using System.Collections.Generic;
using TDL.ContentPlatform.Server.Common.Models.Content;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class FavoriteGetAssetsResponse : ResponseBase
    {
        public List<FavoriteAsset> Assets { get; set; }
    }
}