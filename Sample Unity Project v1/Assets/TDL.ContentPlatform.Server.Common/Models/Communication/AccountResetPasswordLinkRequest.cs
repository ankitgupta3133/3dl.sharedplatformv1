﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountResetPasswordLinkRequest
    {
        public string ResetPasswordToken { get; set; }
        public string Username { get; set; }
        public string NewPassword { get; set; }
    }
}