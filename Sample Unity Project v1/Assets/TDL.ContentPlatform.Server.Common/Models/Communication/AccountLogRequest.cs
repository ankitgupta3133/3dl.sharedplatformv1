﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountLogRequest
    {
        /// <summary>
        /// Unique identifier for this package
        /// </summary>
        public Guid Id { get; set; }
        /// <summary>
        /// Unique identifier for this session
        /// </summary>
        public Guid Session { get; set; }

     
    }
}