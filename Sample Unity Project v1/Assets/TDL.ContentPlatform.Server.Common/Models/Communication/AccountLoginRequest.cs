﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Runtime.CompilerServices;
using TDL.ContentPlatform.Server.Common.Annotations;
using TDL.ContentPlatform.Server.Common.Enums;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
   
    public class AccountLoginRequest:INotifyPropertyChanged
    {
        private string _username;
        private string _password;

        /// <summary>
        /// Username or e-mail address of user.
        /// </summary>
        /// <remarks>If e-mail is used it will be resolved to username before login is attempted.</remarks>
        public string Username
        {
            get => _username;
            set
            {
                if (value == _username) return;
                _username = value;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get => _password;
            set
            {
                if (value == _password) return;
                _password = value;
                OnPropertyChanged();
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}