﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountResetPasswordLinkResponse : ResponseBase
    {
        public List<ErrorCodeDescription> Errors { get; set; }
    }
}