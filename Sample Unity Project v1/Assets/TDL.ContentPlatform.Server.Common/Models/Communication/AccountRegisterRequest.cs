﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountRegisterRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string[] Roles { get; set; }
    }
}