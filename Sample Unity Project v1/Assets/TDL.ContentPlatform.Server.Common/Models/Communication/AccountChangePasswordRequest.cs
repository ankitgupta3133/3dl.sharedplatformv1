﻿namespace TDL.ContentPlatform.Server.Common.Models.Communication
{
    public class AccountChangePasswordRequest
    {
        public string Username { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }
}