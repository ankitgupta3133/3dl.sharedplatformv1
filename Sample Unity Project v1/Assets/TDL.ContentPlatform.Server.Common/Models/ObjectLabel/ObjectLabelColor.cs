﻿namespace TDL.ContentPlatform.Server.Common.Models.ObjectLabel
{
    public class ObjectLabelColor
    {
        public string Highlight { get; set; }
        public string Original { get; set; }
        
    }
}