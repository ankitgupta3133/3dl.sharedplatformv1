﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.ObjectLabel
{
    public class ObjectLabelLanguageText
    {
        public Dictionary<string, string> Languages { get; set; } = new Dictionary<string, string>();

        // Here we can add known languages
        [JsonIgnore]
        public string NO
        {
            get { lock (Languages) return Languages["NO"]; }
            set
            {
                lock (Languages)
                    if (Languages.ContainsKey("NO"))
                        Languages["NO"] = value;
                    else
                        Languages.Add("NO", value);
            }
        }
        [JsonIgnore]
        public string EN
        {
            get { lock (Languages) return Languages["EN"]; }
            set
            {
                lock (Languages)
                    if (Languages.ContainsKey("EN"))
                        Languages["EN"] = value;
                    else
                        Languages.Add("EN", value);
            }
        }
        [JsonIgnore]
        public string AR
        {
            get { lock (Languages) return Languages["AR"]; }
            set
            {
                lock (Languages)
                    if (Languages.ContainsKey("AR"))
                        Languages["AR"] = value;
                    else
                        Languages.Add("AR", value);
            }
        }

        // Default indexer
        public string this[string language]
        {
            get
            {
                lock (Languages)
                    return Languages[language];
            }
            set
            {
                lock (Languages)
                    if (Languages.ContainsKey(language))
                        Languages[language] = value;
                    else
                        Languages.Add(language, value);
            }
        }
    }
}