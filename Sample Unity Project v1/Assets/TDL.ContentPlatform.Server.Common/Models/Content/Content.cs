﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using TDL.ContentPlatform.Server.Common.Annotations;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class Content: INotifyPropertyChanged
    {
        /// <summary>
        /// Contains list of content packages, usually downloaded from server. But could also be locally cached version.
        /// </summary>
        public List<ContentPackage> ContentPackages { get; set; } = new List<ContentPackage>();
        public List<ContentPackage> AssetPackage{ get; set; } = new List<ContentPackage>();

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}