﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientSubtopic
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}