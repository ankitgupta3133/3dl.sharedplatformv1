﻿using System;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientTopic
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public List<ClientSubtopic> Subtopics { get; set; }
    }
}