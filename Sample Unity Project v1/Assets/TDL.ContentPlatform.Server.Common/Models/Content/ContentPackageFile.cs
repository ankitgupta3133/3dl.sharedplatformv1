﻿using System;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ContentPackageFile
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public string Url { get; set; }
    }
}