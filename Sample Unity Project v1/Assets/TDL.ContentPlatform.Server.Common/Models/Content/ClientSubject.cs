﻿using System;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientSubject
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<ClientTopic> Topics { get; set; }
    }
}