﻿namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientContentDetail
    {
        public string LanguageShortName { get; set; }
        public string WhatsNew { get; set; }
        public string Description { get; set; }
    }
}