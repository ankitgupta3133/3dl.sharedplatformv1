﻿using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ClientAssetContent
    {
        public int Id { get; set; }
        public string FileName { get; set; }       
        public int Version { get; set; }
        public string Url { get; set; }
        public string Thumbnail { get; set; }
        public List<ClientContentDetail> Details { get; set; }
    }
}