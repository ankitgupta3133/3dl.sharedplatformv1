﻿using System;
using System.Collections.Generic;

namespace TDL.ContentPlatform.Server.Common.Models.Content
{
    public class ContentPackage
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public long Size { get; set; }
        public Status status { get; set; }        
        public float CurrentVersion { get; set; }
        public float AvailableVersion { get; set; }
        public DateTime PublishedDate { get; set; }
        public string Description { get; set; }
       
        public List<ContentPackageFile> Files { get; set; }
        public List<string> MenuItems { get; set; }
        public string Viewer { get; set; }
        public string MenuType { get; set; }

        public enum Status
        {
            Installed,
            Downloaded,            
            DownloadInProgress,
            NotStarted
        }
    }
}
