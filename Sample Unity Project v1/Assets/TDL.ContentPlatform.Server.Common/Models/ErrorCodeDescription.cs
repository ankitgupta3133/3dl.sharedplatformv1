﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TDL.ContentPlatform.Server.Common.Models
{
    public class ErrorCodeDescription
    {
        public string Code { get; }
        public string Description { get; }

        public ErrorCodeDescription() { }

        public ErrorCodeDescription(string code, string description)
        {
            Code = code;
            Description = description;
        }
    }
}
