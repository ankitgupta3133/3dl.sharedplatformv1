﻿namespace TDL.ContentPlatform.Server.Common.Enums
{
    public enum DataTypeEnum
    {
        Unknown = 0,
        String = 1,
        Long = 2,
        Double = 3
    }
}
