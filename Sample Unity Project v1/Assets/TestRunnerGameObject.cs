﻿using System.Collections;
using System.Collections.Generic;
using TDL.ContentPlatform.Server.Common;
using TDL.ContentPlatform.Server.Common.Models.Communication;
using UnityEditor.PackageManager;
using UnityEngine;

public class TestRunnerGameObject : MonoBehaviour
{
    public string Host = "https://api-v1.3dl.no";
    public string Username = "Student1";
    public string Password = "Coffe is good1!";

	// Use this for initialization
	void Start () {
	    var auth = SampleHttpRestClient.DoHttp<AccountLoginRequest, AccountLoginResponse>($"{Host}/api/v1/Account/Login", SampleHttpRestClient.HttpMethods.GET, new AccountLoginRequest() { Username = Username, Password = Password });
	    var package = SampleHttpRestClient.DoHttp<ContentPackageListRequest, ContentPackageListResponse>($"{Host}/api/v1/Content/Packages", SampleHttpRestClient.HttpMethods.GET, new ContentPackageListRequest() { }, auth.AuthorizationToken);

	    if (package.Success)
	    {
	        Debug.Log("Found: " + package.Packages[0].Name);
	    }
	    else
	    {
	        Debug.LogError($"Error: {package.ErrorMessage ?? "<null>"}");
	    }

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
